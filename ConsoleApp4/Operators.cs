﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ConsoleApp4
{
    public static class Calculator
    {
        private static double _result;
        private static string _lastOperator;
        public static bool IsFinish;
        private static List<string> _historyList = new List<string>();

        public static void Calculate(double currentNumber, string nextOperator)
        {
            if (string.IsNullOrEmpty(_lastOperator))
            {
                Default(currentNumber);
            }
            else
            {
                switch (_lastOperator)
                {
                    case OperatorTypes.Plus: Plus(currentNumber); break;
                    case OperatorTypes.Minus: Minus(currentNumber); break;
                    case OperatorTypes.Divide: Divide(currentNumber); break;
                    case OperatorTypes.Multi: Multi(currentNumber); break;
                    case OperatorTypes.Cos: Cos(currentNumber); break;
                }
            }

            _lastOperator = nextOperator;

            if (_lastOperator == OperatorTypes.Result)
                IsFinish = true;

            AddToHistory(currentNumber, _lastOperator);
        }

        public static void Reset()
        {
            _result = 0;
            _lastOperator = string.Empty;
            IsFinish = false;
            _historyList.Clear();
        }

        private static void Default(double currentNumber)
        {
            _result = currentNumber;
        }

        private static void Plus(double currentNumber)
        {
            _result += currentNumber;
        }

        private static void Minus(double currentNumber)
        {
            _result -= currentNumber;
        }

        private static void Divide(double currentNumber)
        {
            _result /= currentNumber;
        }

        private static void Multi(double currentNumber)
        {
            _result *= currentNumber;
        }

        private static void Cos(double currentNumber)
        {
            _result = Math.Cos(currentNumber);
        }

        public static double Result()
        {
            return _result;
        }

        private static void AddToHistory(double currentNumber, string lastOperator)
        {
            var range = new[]
            {
                currentNumber.ToString(CultureInfo.CurrentCulture), lastOperator
            };

            _historyList.AddRange(range);
        }

        public static string GetHistory()
        {
            var history = new StringBuilder();

            foreach (var currentRecord in _historyList)
            {
                history.Append($" {currentRecord} ");
            }

            return history.ToString();
        }

        public static string GetHistoryWithResult()
        {
            return GetHistory() + _result;
        }
    }
}
