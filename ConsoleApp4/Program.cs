﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp4
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var again = true;

            while (again)
            {
                while (!Calculator.IsFinish)
                {
                    double _number = 0;
                    var isNumber = false;

                    var _operator = string.Empty;
                    var isOperator = false;

                    // check number 
                    while (!isNumber)
                    {
                        Console.Write("Введите число:");
                        isNumber = Validate.IsNumber(Console.ReadLine(), out _number);
                    }

                    // check operator
                    while (!isOperator)

                    {
                        Console.Write("Введите операцию  + , - , * , / , cos , sin , tan , cat:   ");
                        var currentOperator = Console.ReadLine();

                        if (Validate.IsOperator(currentOperator))
                        {
                            isOperator = true;
                            _operator = currentOperator;
                        }
                    }

                    Calculator.Calculate(_number, _operator);

                    // current result
                    //Console.WriteLine("current result: " + Calc.Result());
                }

                // result
                Console.WriteLine();
                Console.WriteLine("Result: " + Calculator.GetHistoryWithResult());

                // again ?
                Console.WriteLine();
                Console.WriteLine("Для повторного вычисления, введите (y),");
                Console.WriteLine();
                Console.WriteLine("Для завершения, введите любой символ.");

                var yesOrNo = Console.ReadLine();
                if (yesOrNo == "y" || yesOrNo == "Y")
                {
                    Calculator.Reset();
                }
                else
                {
                    again = false;
                }
            }
        }
    }
}
