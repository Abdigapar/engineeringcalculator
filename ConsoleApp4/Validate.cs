﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp4
{
    public static class Validate
    {
        public static bool IsOperator(string currentOperator)
        {
            return OperatorTypes.AvailableOperators.Contains(currentOperator);
        }

        public static bool IsNumber(string currentNumber, out double x)
        {
            return double.TryParse(currentNumber, out x);
        }
    }
}
