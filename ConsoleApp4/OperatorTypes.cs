﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp4
{
    public static class OperatorTypes
    {
        public const string Plus = "+";
        public const string Minus = "-";
        public const string Multi = "*";
        public const string Divide = "/";

        public const string Cos = "cos";

        public const string Result = "=";

        public static readonly string[] AvailableOperators =
        {
            Plus, Minus, Multi, Divide, Cos, Result
        };
    }
}
